Places API based discovery engine

Setup instructions:
1. Install NodeJS, MongoDB
2. Create DB from db-files directory
    mongoimport --db housing-hack --collection popularity --file popularityDb.json
3. Run the node server file -
    From the root directory of the project, Run
    node app.js
4. Run the app file index.html