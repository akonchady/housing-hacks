var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var totalAmountSchema = new Schema({
    "user_id":Number,
    "pay":Number,
    "receive":Number,
    "balance":Number
});
module.exports = mongoose.model('total_amt', totalAmountSchema,'total_amt');