var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var regionSchema = new Schema({
    "_id":Number,
    "Ward_Name":String,
    "Lat":String,
    "long":String

});
module.exports = mongoose.model('regions', regionSchema,'regions');