var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    "_id":Number,
    "user_name" : String,
    "nickname" : String,
    "password" : String,
    "registered_date" : Number,
    "is_active" : Boolean
});
module.exports = mongoose.model('user_info', userSchema,'user_info');