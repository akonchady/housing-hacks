var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var popularitySchema = new Schema({
    "Ward_Name":String,
    "Lat":String,
    "long":String,
    "distanceValue":Number,
    "type":Array
});
module.exports = mongoose.model('popularity', popularitySchema,'popularity');