var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var expenseSchema = new Schema({
    "_id":Number,
    "user_id" : Number,
    "type" : String,
    "actors" : Array,
    "amount" : Number,
    "actual_amount" : Number,
    "description" : String,
    "category" : String,
    "last_modified" : Number,
    "isDeleted" : Boolean
});
module.exports = mongoose.model('expense', expenseSchema,'expense');