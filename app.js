var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    routes = require('./routes/index'),
    mongoose = require('mongoose'),
    Regions = require('./models/regions_model.js'),
    dbURI = 'mongodb://localhost:27017/housing-hack',// Build the connection string
    Popularity = require('./models/popularity_model.js');
    app = express()
var distance = require('google-distance');
var config = require("./config.js");

var GooglePlaces = require("googleplaces");
var googlePlaces = new GooglePlaces(config.apiKey, config.outputFormat);
var parameters;


// Create the database connection
mongoose.connect(dbURI);
module.exports.mongoose = mongoose;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Cache-Control");
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
});

app.get('/getRegions', function (req, res) {
    Regions.find({}, function (err, docs) {
        if (err) {
            res.send(err);
            return;
        }
        if (docs) {
            //console.log(docs);

            //for(var i=0;i<docs.length;i++)
            //{
            //    console.log(i +"-"+ docs[i].Lat +"-"+ docs[i].long);
            //    //setTimeout(function(i) {
            //        parameters = {
            //            location:[docs[i].Lat, docs[i].long],
            //            types:"food|health|",
            //            radius:"200"
            //        };
            //
            //        googlePlaces.placeSearch(parameters, function (err, response) {
            //            console.log(i, JSON.stringify(response));
            //        });
            //    //}(i));
            //
            //
            //}
            var i = 0,j = 0;
            outerFunction(i, docs);
            function outerFunction(i, docs) {
                console.log(i + "-" + docs[i].Lat + "-" + docs[i].long);

                parameters = {
                    location: [docs[i].Lat, docs[i].long],
                    types: "food|health|bar|restaurant|amusement_park|bowling_alley|cafe|night_club|atm|bakery|bank|school|gym|gas station|shopping_mall|train_station|spa|park|police|hospital|health|",
                    radius: "500"
                };

                googlePlaces.placeSearch(parameters, function (err, items) {

                    j = 0;

                    distanceInnerFunction(items,j);
                    function distanceInnerFunction(items) {
                        console.log(i);
                        if (typeof(items)!='undefined' && items.results.length !=0 && items.results[j].geometry) {
                            distance.get(
                                {
                                    index: 1,
                                    origin: '' + docs[i].Lat + ',' + docs[i].long + '',
                                    destination: '' + items.results[j].geometry.location.lat + ',' + items.results[j].geometry.location.lng
                                },
                                function (err, data) {
                                    if (err) return console.log(err);
                                    if(items.results[j])
                                        savePopularity(data,items.results[j],docs[i]);


                                    //console.log(data);
                                    myInnerFunction();
                                });
                        }

                    }

                    function myInnerFunction() {
                        j++;
                        if (j < items.results.length) {
                            distanceInnerFunction(items,j);
                        }
                    }


                    myFunction();
                });
            }

            function myFunction() {
                i++;
                if (i < docs.length) {
                    outerFunction(i, docs);
                }
            }

            function savePopularity(data,items,docs) {

                var obj = {
                    "Ward_Name":docs.Ward_Name,
                    "Lat":items.geometry.location.lat,
                    "long":items.geometry.location.lng,
                    "distanceValue":data.distanceValue,
                    "type":items.types
                };
                var popularity = new Popularity(obj);
                popularity.save(function (err, res) {
                console.log(JSON.stringify(res));

                });
            }
            //getNewExpenseId(sequenceSuccess);

            //var arr  =  docs.map(function(item) {
            //     return {
            //         key : 0,
            //         val : (function(item) {
            //             parameters = {
            //                 location:[item.Lat, item.lang],
            //                 types:"food|health|",
            //                 radius:"200"
            //             };
            //
            //             googlePlaces.placeSearch(parameters, function (err, response) {
            //                 console.log(JSON.stringify(response));
            //                 return response;
            //             });
            //         })(item)
            //     }
            //  });
            /*
             for(var i=0;i<docs.results.length;i++)
             {
             parameters = {
             location:[docs.results[i].Lat, docs.results[i].lang],
             types:"food|health|",
             radius:"200"
             };




             googlePlaces.placeSearch(parameters, function (err, response) {
             console.log(JSON.stringify(response));
             });
             }*/


            //  module.exports = router;


            //res.json(docs);
            //console.log(JSON.stringify(docs));


        }
        else {
            res.json({status: -1});
        }
    });
});


/*db.popularity.aggregate( [ { $unwind : "$type" }, {
    $group:{
        _id:{"Ward":"$Ward_Name", "type":"$type"},
        "Weightage":{"$avg":{"$multiply":["$distanceValue",0.00021773]}
        }
    }}] )*/
app.get('/getPopularRegionsForUserType', function (req, res) {
    /*Popularity.find({}, function (err, docs) {
        if (err) {

            res.send(err);
            return;
        }
        if (docs) {
            res.json(docs);
        }
        else {
            res.json({status: -1});
        }
    });*/

    Popularity.aggregate({ $unwind : "$type" }, {
        $group:{
            _id:{"Ward":"$Ward_Name"}, //"type":"$type"
            "Weightage":{"$avg":{"$multiply":["$distanceValue",0.00021773]}
            }
        }},
        {$sort : {"Weightage" : 1}},
        {$limit : 100}
        , function(err, docs) {
        if (err) {
            res.send(err);
            return;
        }
        if (docs) {
            res.json(docs);
        }
        else {
            res.json({status: -1});
        }
    });
});

app.get('/getPopularRegionsForNonCustomTypes', function (req, res) {
    /*Popularity.find({}, function (err, docs) {
     if (err) {

     res.send(err);
     return;
     }
     if (docs) {
     res.json(docs);
     }
     else {
     res.json({status: -1});
     }
     });*/

    Popularity.aggregate({ $unwind : "$type" }, {
            $group:{
                _id:{"Ward":"$Ward_Name"}, //"type":"$type",
                $match : {type : {$in : ['bar', 'movie_theater', 'bowling_alley', 'night_club', 'cafe']}}
                //"counts":{"count":{"$multiply":["$distanceValue",0.00021773]}
                //}
            }},
        //{$sort : {"Weightage" : 1}},
        {$limit : 100}
        , function(err, docs) {
            if (err) {
                res.send(err);
                return;
            }
            if (docs) {
                res.json(docs);
            }
            else {
                res.json({status: -1});
            }
        });
});

app.get('/getLatLongForARegion', function (req, res) {
    Regions.find({Ward_Name: req.query.wardName}, function (err, docs) {
        if (err) {
            res.send(err);
            return;
        }
        if (docs) {
            res.json(docs);
        }
        else {
            res.json({status: -1});
        }
    });
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/*Make the mongoose connection*/


app.listen(3352);

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

// CONNECTION EVENTS

console.log("Server listening at port 3352");

// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbURI);
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});