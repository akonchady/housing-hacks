
var express = require('express'),
    router = express.Router(),
    Users = require('../models/user_model.js'),
    Counters = require('../models/counters_model.js'),
    Total_amt = require('../models/total_amt_model.js');

function getNewUserId(callback)
{
    Counters.findByIdAndUpdate("user_id" , { $inc: { "sequence_value": 1 } },callback);
}

function insertInitialBalance(userId,callback)
{
    var total_amt = new Total_amt({
        "user_id":userId,
        "pay":0,
        "receive":0,
        "balance":0
    });
    total_amt.save(callback);
}

router.get('/', function(req, res) {
    Users.find({} , {_id:1,user_name:1,nickname:1,registered_date:1,is_active:1,group_id:1}, function (err, docs) {
        res.send(docs);
    });
});

router.post('/signup', function(req, res) {
    if(!req.body.user_name && !req.body.password){
        res.send({status:-1,message:"wrong request"});
        return;
    }
    // Check for Duplicate User
    Users.findOne({user_name:req.body.user_name},function(err,doc){
        if(err){

        }
        if(doc)
        {
            res.json({status: -2,message:"User already exists"});
        }
        else
        {
            function sequenceSuccess(err,data) {
                if (err) {
                    res.send(err);
                    return;
                }
                var obj = {
                    "_id" : data.sequence_value,
                    "user_name" : req.body.user_name,
                    "nickname" : req.body.nickname,
                    "password" : req.body.password,
                    "registered_date" :req.body.registered_date,
                    "is_active" : true
                };
                var users = new Users(obj);
                users.save(function (err, docs) {
                    insertInitialBalance(data.sequence_value,function(err,data){
                        if (err) {
                            res.send(err);
                            return;
                        }
                        if (docs) {
                            res.json({status: 0,user_id:obj._id});
                        }
                        else {
                            res.json({status: -1});
                        }

                    });
                });
            }

            getNewUserId(sequenceSuccess);
        }

    })
});

router.post('/validate', function(req, res) {
    Users.findOne({user_name:req.body.user_name,password:req.body.password,is_active:true}, function (err, docs) {
        if(err)
        {
            res.send(err);
            return;
        }
        if(docs) {
            res.json({_id: docs._id,nickname:docs.nickname,registeredDate:docs.registered_date,status:0,group_id:docs._doc.group_id});
        }
        else
        {
            res.json({status: -1});
        }
    });
});

module.exports = router;