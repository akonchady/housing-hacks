var express = require('express'),
    app = require("../app.js"),
    router = express.Router(),
    Regions = require('../models/regions_model.js');




var config = require("./../config.js");
//var mongoose = require('mongoose');
//var db = mongoose.connect('mongodb://localhost/housing-hacks');


var GooglePlaces = require("googleplaces");
var googlePlaces = new GooglePlaces(config.apiKey, config.outputFormat);
var parameters;

/**
 * Place search - https://developers.google.com/places/documentation/#PlaceSearchRequests
 */


router.post('/getRegions', function(req, res) {
    console.log('hhh');
    if(!req.body.user_id){
        res.send({status:-1,message:"wrong request"});
        return;
    }
    Regions.find({}, function (err, docs) {
        if(err)
        {
            res.send(err);
            return;
        }
        if(docs) {
            res.json(docs);
            console.log(JSON.stringify(docs));
        }
        else
        {
            res.json({status: -1});
        }
    });
});

parameters = {
    location:[-33.8670522, 151.1957362],
    types:"restaurant|health",
    radius:"200"
};

module.exports = router;

//googlePlaces.placeSearch(parameters, function (err, response) {
//    //console.log(JSON.stringify(response));
//});