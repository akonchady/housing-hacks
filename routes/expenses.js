var express = require('express'),
    app = require("../app.js"),
    router = express.Router(),
    Expenses = require('../models/expense_model.js'),
    Counters = require('../models/counters_model.js'),
    Total_amt = require('../models/total_amt_model.js'),
    Regions = require('../models/regions_model.js');



function getNewExpenseId(callback)
{
    Counters.findByIdAndUpdate("expenseId" , { $inc: { "sequence_value": 1 } },callback);
}

function updateTotalAmt(user_id,receive,pay,callback)
{
    Total_amt.update({ user_id: { $in: user_id } },{$inc: { "balance": (receive - pay),"pay":pay,"receive":receive }},callback);
}

router.post('/add', function(req, res) {
    if(!req.body.user_id && !req.body.actual_amount){
        res.send({status:-1,message:"wrong request"});
        return;
    }
    req.body.actors = req.body.actors.split(",");

    function sequenceSuccess(err,data) {
        if (err) {
            res.send(err);
            return;
        }
        var obj = {
            _id: data.sequence_value,
            user_id: parseInt(req.body.user_id),
            type: req.body.type,
            actors: req.body.actors,
            amount: (parseInt(req.body.actual_amount) / req.body.actors.length),
            actual_amount: parseInt(req.body.actual_amount),
            description: req.body.description,
            category: req.body.category,
            last_modified: parseInt(req.body.last_modified),
            isDeleted: false
        };
        var expense = new Expenses(obj);
        expense.save(function (err, docs) {
            var receive = 0,pay = 0;
            if(obj.type == "receive")
            {
                receive = obj.amount;
            }
            else
            {
                pay = obj.amount;
            }
            updateTotalAmt([obj.user_id],receive,pay,function(err,data){
                if (err) {
                    res.send(err);
                    return;
                }
                updateTotalAmt(obj.actors,pay,receive,function(err,data){
                    if (err) {
                        res.send(err);
                        return;
                    }
                    if (docs) {
                        res.json({status: 0,id:data.sequence_value});
                    }
                    else {
                        res.json({status: -1});
                    }
                });

            });

        });
    }
    getNewExpenseId(sequenceSuccess);
});

router.post('/getBalance', function(req, res) {
    if(!req.body.user_id){
        res.send({status:-1,message:"wrong request"});
        return;
    }
    Total_amt.findOne({user_id:req.body.user_id}, function (err, docs) {
        if(err)
        {
            res.send(err);
            return;
        }
        if(docs) {
            res.json({balance: docs.balance,pay:docs.pay,receive:docs.receive,status:0});
        }
        else
        {
            res.json({status: -1});
        }
    });
});


router.post('/getHistory', function(req, res) {
    if(!req.body.user_id){
        res.send({status:-1,message:"wrong request"});
        return;
    }
    var pageNumber = req.body.pageNumber ? req.body.pageNumber : 1;
    var limit = req.body.count ? req.body.count : 10;
    var skip = (pageNumber-1)*limit;
    Expenses.find({$and:[{$or:[{user_id:req.body.user_id},{actors:req.body.user_id}]},{isDeleted:false}]},{
        "type" : 1,
        "user_id":1,
        "amount" : 1,
        "description" :1,
        "category" :1,
        "last_modified" : 1,
        "actors":1,
        "_id":1
    },{
        skip:skip, // Starting Row
        limit:limit, // Ending Row
        sort:{
            last_modified: -1 //Sort by Date Added DESC
        }
    }, function (err, docs) {
        if(err)
        {
            res.send(err);
            return;
        }
        if(docs) {
            for(var i=0;i<docs.length;i++){
                if(docs[i].actors.indexOf(req.body.user_id) !== -1)
                {
                    docs[i].type = docs[i].type == "pay" ? "receive" : "pay";
                    docs[i].actors = [docs[i].user_id.toString()];
                }
            }

            res.send(docs);
        }
        else
        {
            res.json({status: -1});
        }
    });
});

/*This will give the expenses for every other user in the group with respect to the user sending the request*/
router.post('/getExpenseDueForUser', function(req, res) {
    if(!req.body.user_id){ //Take the group id as well here.. since we should check expense details only within that group.
        res.send({status:-1,message:"wrong request"});
        return;
    }
    var userId = req.body.user_id;
    if(typeof user_id === 'string') {
        userId = parseInt(userId);
    }

    Expenses.find({$and : [{$or : [{actors : userId}, {user_id : userId}]}, {isDeleted : false}]}, {actors:1,amount:1,type:1,user_id:1}, {}, function(err, docs) {
        var expenseObj = {},userId = parseInt(req.body.user_id);

        function storeExpense(value, inc) {
            if(!expenseObj[value])
                expenseObj[value]  = 0;

            if(inc)
                expenseObj[value]  += doc.amount;
            else
                expenseObj[value]  -= doc.amount;
        }

        for(var i=0;i<docs.length;i++) {
            var doc = docs[i]._doc;

            if(userId === doc.user_id) { //So, take values to add/subtract from actors array
                if(doc.type === 'receive') { //then pay
                    for(var j=0;j<doc.actors.length;j++) {
                        storeExpense(doc.actors[j], true);
                    }
                }
                else {
                    for(var j=0;j<doc.actors.length;j++) {
                        storeExpense(doc.actors[j], false);
                    }
                }
            }
            else {//This means userId is in actors
                if(doc.type === 'receive') { //then pay
                    storeExpense(doc.user_id, false);
                }
                else {
                    storeExpense(doc.user_id, true);
                }
            }
        }
        res.send(expenseObj);
    });
});

module.exports = router;