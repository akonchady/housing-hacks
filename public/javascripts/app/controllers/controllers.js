var homeCtrl = function ($scope, $modal, ApiCall) {
    $scope.data = "App ctrl";
    var modalInstance;
    $scope.modalObject = {
      name : '',
        lat: null,
        long: null
    };

    $scope.filterRegions = function (regionId) {

    };
    $scope.openUserCategoryPopup = function () {
        modalInstance = $modal.open({
            templateUrl: 'public/views/userCategoryPopup.html',
            backdrop: 'static',
            scope: $scope
        });
    };

    $scope.openUserCategoryPopup();


    $scope.loadRegions = function(userType) {
        $scope.cancel();
        $scope.showLoader();
        ApiCall.loadRegions(userType, function(data) {
            $scope.regions =data;

            console.log(data);
            $scope.filters = [
                {
                    id: 1,
                    value: "Commute"
                }, {
                    id: 2,
                    value: "Healthcare"
                }, {
                    id: 3,
                    value: "Education"
                }, {
                    id: 4,
                    value: "Restaunrts"
                }
            ];

        $scope.hideLoader();
        }, function(err) {
            console.log(err);
        });
    }

    $scope.showLoader= function () {
        angular.element(".loader-img").show();
    }
    $scope.hideLoader= function () {
        angular.element(".loader-img").hide();
    }

    $scope.loadRegionPopup= function (index ) {
        //alert("hi");
        $scope.firstCity=$scope.regions[index].Ward_Name;
        //console.log( $scope.regions[index].Lat+" -"+ $scope.regions[index].long);
        modalInstance = $modal.open({
            templateUrl: 'public/views/regionDetails.html',
            backdrop: 'static',
            scope: $scope
        });

            setTimeout(function () {
                callingGoogleMap(index);
            },3000)

    }
    function callingGoogleMap(index){
        var mapOptions = {
            center: {lat:  13.5800, lng: 77.8000},
            zoom: 8
        };
        var map = new google.maps.Map(document.getElementById('map-canvas1'),
            mapOptions);

        var marker = new google.maps.Marker({
            position: { lat : $scope.regions[index].Lat, long : $scope.regions[index].long},
            map: map,
            title:  $scope.regions[index].Ward_Name
            //icon:'../../public/images/homelogo.png'
        });

        google.maps.event.addDomListener(window, 'load');

    }
    $scope.cancel = function() {
        modalInstance.dismiss('cancel');
    };
};
/*var ModalInstanceCtrl = function($scope, $modalInstance, ApiCall) {

};*/

app.controller("homeCtrl", homeCtrl);/*
app.controller("ModalInstanceCtrl", ModalInstanceCtrl);*/
