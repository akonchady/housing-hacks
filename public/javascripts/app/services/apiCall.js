app.factory('ApiCall', function($http) {
    return {
        loadRegions : function(data, successCallback, errorCallback) {
            $http.get('http://localhost:3352/getPopularRegionsForUserType').
                success(successCallback).
                error(errorCallback);
        }
    }
});